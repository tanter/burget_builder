import React, { Component } from 'react';
import { connect } from 'react-redux';

import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import Loader from '../../components/UI/Loader/Loader';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import * as burgerBuilderActions from '../../store/actions/index';

class BurgerBuilder extends Component
{
  state = {
    purchasable: false,
    purchasing: false
  };

  componentDidMount = () => {
    if (!this.props.ingr) {
      this.props.onInitIngredients();
    }
  };

  updatePurchasableState = ingredients => {
    const sum = Object.keys(ingredients).map(key => {
      return ingredients[key];
    }).reduce((prev, current) => {
      return prev + current;
    }, 0);
    return sum > 0;
  };

  purchasingHandler = () => {
    if (this.props.isAuth) {
      this.setState({
        purchasing: true
      });
    } else {
      this.props.history.push('/auth');
    }
  };

  purchaseCancelHandler = () => {
    this.setState({
      purchasing: false
    });
  };

  purchaseContinueHandler = () => {
    this.props.history.push('/checkout');
  };

  render() {
    const disableInfo = {...this.props.ingr};
    for (let key in disableInfo) {
      disableInfo[key] = disableInfo[key] <= 0;
    }

    let burger = this.state.error 
      ? <p style={{margin: '15px 20px', textAlign: 'center'}}>Something went wrong!</p>
      : <Loader/>;

    if (this.props.ingr) {
      burger = (
        <React.Fragment>
          <Burger ingredients={this.props.ingr} />
          <BuildControls
            isAuth={this.props.isAuth}
            price={this.props.price}
            ingredients={this.props.ingr}
            addIngredient={this.props.onAddIngredient}
            removeIngredient={this.props.onRemoveIngredient}
            purchasable={this.updatePurchasableState(this.props.ingr)}
            purchasing={this.purchasingHandler}
          />
        </React.Fragment>
      );
    }

    return(
      <React.Fragment>
        <Modal
          show={this.state.purchasing}
          modalClose={this.purchaseCancelHandler}
        >
          <OrderSummary
            totalPrice={this.props.price}
            ingredients={this.props.ingr ? this.props.ingr : {}}
            purchaseCancel={this.purchaseCancelHandler}
            purchaseContinue={this.purchaseContinueHandler}
          />
        </Modal>
        {burger}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    ingr: state.burgerBuilder.ingredients,
    price: state.burgerBuilder.totalPrice,
    err: state.burgerBuilder.error,
    isAuth: state.auth.token != null
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onAddIngredient: (ingredientName) => dispatch(burgerBuilderActions.addIngredient(ingredientName)),
    onRemoveIngredient: (ingredientName) => dispatch(burgerBuilderActions.removeIngredient(ingredientName)),
    onInitIngredients: () => dispatch(burgerBuilderActions.initIngredients()),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(BurgerBuilder);
