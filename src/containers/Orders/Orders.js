import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';

import Order from '../../components/Order/Order';
import Loader from '../../components/UI/Loader/Loader';

class Orders extends Component
{
  state = {
    orders: null,
    error: null
  };

  componentDidMount() {
    const orders = [];
    axios.get('https://burger-builder-367e1.firebaseio.com/orders.json?auth=' + this.props.token)
      .then(res => {
        for (let key in res.data) {
          orders.push({
            id: key,
            ...res.data[key]
          });
        }
        this.setState({orders});
      })
      .catch(err => {
        this.setState({error: err.message});
      });
  }

  render() {
    return(
      <div>
        {this.state.error ? <h2 style={{textAlign: 'center'}}>{this.state.error}</h2> :
          this.state.orders ?
            this.state.orders.length ?
              this.state.orders.map(order => {
                return(
                  <Order
                    key={order.id}
                    price={order.price}
                    ingredients={order.ingredients}
                  />
                );
              })
              :
              <h2 style={{textAlign: 'center'}}>Nothing to show</h2>
            :
            <Loader/>
        }
      </div>
    );
  };
}

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps)(Orders);
