import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import Button from '../../../components/UI/Button/Button';
import Input from '../../../components/UI/Input/Input';
import Loader from '../../../components/UI/Loader/Loader';
import * as orderActions from '../../../store/actions/index';
import './ContactData.css';

class ContactData extends Component
{
  state = {
    formIsValid: false,
    orderForm: {
      name: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Name'
        },
        value: '',
        valid: false,
        touched: false,
        validation: {
          require: true
        }
      },    
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Email'
        },
        value: '',
        valid: false,
        touched: false,
        validation: {
          require: true
        }
      },
      street: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Street'
        },
        value: '',
        valid: false,
        touched: false,
        validation: {
          require: true
        }
      },
      postal: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Postal'
        },
        value: '',
        valid: false,
        touched: false,
        validation: {
          require: true,
          minLength: 5,
          maxLength: 5,
        },
      },
      deliveryMethod: {
        elementType: 'select',
        elementConfig: {
          options: [
            {value: 'fast', displayValue: 'Fast'},
            {value: 'cheap', displayValue: 'Cheap'},
          ]
        },
        validation: {},
        dontValid: true,
        valid: true
      }
    }
  };

  inputChangedHandler = (event, inputId) => {
    const updatedOrderForm = {...this.state.orderForm};
    updatedOrderForm[inputId].value = event.target.value;
    updatedOrderForm[inputId].touched = true;
    updatedOrderForm[inputId].valid = this.checkValid(event.target.value, updatedOrderForm[inputId].validation);
    let formValid = true;
    for (let inputIndentifier in updatedOrderForm) {
      formValid = updatedOrderForm[inputIndentifier].valid && formValid;
    }
    this.setState({
      orderForm: updatedOrderForm,
      formIsValid: formValid
    });
  };

  checkValid = (value, rules) => {
    let isValid = true;
    
    if (!rules) return true; 

    if (rules.require) {
      isValid = value.trim() !== '' && isValid;
    }

    if (rules.minLength) {
      isValid = value.length <= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length >= rules.maxLength && isValid;
    }

    return isValid;
  };

  orderHandler = event => {
    event.preventDefault();
    const formData = {};
    for (let item in this.state.orderForm) {
      formData[item] = this.state.orderForm[item].value;
    }

    const data = {
      ingredients: this.props.ings,
      price: this.props.price,
      userId: this.props.userId,
      formData
    };
    console.log(this.props.userId);
    this.props.onOrderBurger(data, this.props.token);
    this.props.history.push('/');
  };

  render() {
    const formInputs = [];
    for (let key in this.state.orderForm) {
      formInputs.push({
        id: key,
        config: this.state.orderForm[key]
      });
    }

    let form = (
      <div className="ContactData">
        <h4>Enter your contact data</h4>
        <form onSubmit={this.orderHandler}>
          {formInputs.map(formEl => (
            <Input 
              key={formEl.id}
              elementType={formEl.config.elementType}
              elementConfig={formEl.config.elementConfig}
              value={formEl.config.value}
              invalid={!formEl.config.valid}
              touched={formEl.config.touched}
              dontValid={formEl.config.dontValid}
              changed={(e) => this.inputChangedHandler(e, formEl.id)}
            />
          ))}
          <Button formIsValid={!this.state.formIsValid} btnType="Success">ORDER</Button>
        </form>
      </div>
    );
    
    if (this.props.loading) {
      form = <Loader/>;
    }

    if (this.props.error) {
      form = <p align={'center'}>Something went wrong</p>
    }

    return form;
  }
}

const mapStateToProps = state => {
  return {
    ings: state.burgerBuilder.ingredients,
    price: state.burgerBuilder.totalPrice,
    loading: state.burgerBuilderOrder.loading,
    ord: state.burgerBuilderOrder.orders,
    error: state.burgerBuilderOrder.error,
    token: state.auth.token,
    userId: state.auth.userId
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onOrderBurger: (orderData, token) => dispatch(orderActions.purchaseBurger(orderData, token)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ContactData));
