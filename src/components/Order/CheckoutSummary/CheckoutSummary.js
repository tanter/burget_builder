import React from 'react';

import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';
import './CheckoutSummary.css';

const checkoutSummary = props => (
    <div className="OrderCheckoutSummary">
      <h1>We hope it tasted well!!</h1>
      <div style={{width: '100%', height: '300px', margin: 'auto'}}>
        <Burger ingredients={props.ingredients ? props.ingredients : {}}></Burger>
      </div>
      <Button btnType="Danger" clicked={props.cancelHandler}>CANCEL</Button>
      <Button btnType="Success" clicked={props.continueHandler}>CONTINUE</Button>
    </div>
);

export default checkoutSummary;
