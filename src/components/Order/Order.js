import React from 'react';

import './Order.css';

const order = props => {
  const ingredients = [];
  for (let key in props.ingredients) {
    if (!+props.ingredients[key]) continue;
    ingredients.push(`<span>${key} (${props.ingredients[key]})</span>`);
  }

  return(  
    <div className="Order">
      <p dangerouslySetInnerHTML={{__html: 'Ingredients: ' + ingredients.join(' ')}}></p>
      <p>Price: <strong>USD {props.price}</strong></p>
    </div>
  );
};

export default order;
