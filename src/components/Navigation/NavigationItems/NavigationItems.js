import React from 'react';

import NavigationItem from './NavigationItem/NavigationItem';
import './NavigationItems.css';

const navigationItems = props => (
  <ul className="NavigationItems">
    <NavigationItem link="/" exact>Burger builder</NavigationItem>
    {!props.isAuth ? null : <NavigationItem link="/orders">Orders</NavigationItem>}
    {!props.isAuth
      ? <NavigationItem link="/auth">Auth</NavigationItem>
      : <NavigationItem link="/logout">Logout</NavigationItem>
    }

  </ul>
);

export default navigationItems;
