import React from 'react';

import './BuildControl.css';

const buildControl = props => (
  <div className="BuildControl">
    <div className="Label">{props.label}: {props.amount}</div>
    <button className="Less" onClick={props.remove} disabled={props.amount <= 0}>Less</button>
    <button className="More" onClick={props.add} disabled={props.amount >= 4}>More</button>
  </div>
);

export default buildControl;
