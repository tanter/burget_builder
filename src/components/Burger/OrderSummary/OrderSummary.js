import React from 'react';

import Aux from '../../../hoc/Auxiliary/Auxiliary';
import Button from '../../UI/Button/Button';

const orderSummary = props => {
  const ingredientsSummary = Object.keys(props.ingredients).map(key => {
    return <li key={key}><span style={{textTransform: 'capitalize'}}>{key}: </span>{props.ingredients[key]}</li>
  });
  return(
    <Aux>
      <h3>Your order</h3>
      <p>A delicious burger with the following ingredients:</p>
      <ul>
        {ingredientsSummary}
      </ul>
      <p>Total price: <strong>{props.totalPrice.toFixed(2)}</strong></p>
      <p>Continue to Checkout?</p>
      <Button clicked={props.purchaseCancel} btnType={'Danger'}>CANCEL</Button>
      <Button clicked={props.purchaseContinue} btnType={'Success'}>CONTINUE</Button>
    </Aux>
  );
};

export default orderSummary;
