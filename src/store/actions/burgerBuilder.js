import axios from 'axios';

import * as actionTypes from './actionTypes';

export const addIngredient = ingredientName => {
  return {
    type: actionTypes.ADD_INGREDIENT,
    ingredientName
  }
};

export const removeIngredient = ingredientName => {
  return {
    type: actionTypes.REMOVE_INGREDIENT,
    ingredientName
  }
};

export const setIngredients = ingredients => {
  return {
    type: actionTypes.SET_INGREDIENT,
    ingredients: {
      salad: ingredients.salad,
      cheese: ingredients.cheese,
      meat: ingredients.meat,
      bacon: ingredients.bacon,
    },
    totalPrice: 4
  };
};

export const errorFetchIngredients = () => {
  return {
    type: actionTypes.ERROR_INGREDIENT
  };
};

export const initIngredients = () => dispatch => {
  axios.get('https://burger-builder-367e1.firebaseio.com/ingredients.json')
    .then(res => {
      dispatch(setIngredients(res.data))
    })
    .catch(err => {
      dispatch(errorFetchIngredients())
    });
};
