import axios from 'axios';
import * as actionTypes from './actionTypes';

export const purchaseBurgerSuccess = (id, orderData) => {
  return {
    type: actionTypes.PURCHASE_BURGER_SUCCESS,
    orderId: id,
    orderData
  }
};

export const purchaseInit = () => {
  return {
    type: actionTypes.PURCHASE_INIT
  };
};

export const purchaseBurgerFail = error => {
  return {
    type: actionTypes.PURCHASE_BURGER_FAIL,
    error: true
  }
};

export const purchaseBurgerStart = () => {
  return {
    type: actionTypes.PURCHASE_BURGER_START
  }
};

export const purchaseBurger = (orderData, token) => {
  return dispatch => {
    dispatch(purchaseBurgerStart());
    axios.post('https://burger-builder-367e1.firebaseio.com/orders.json?auth=' + token, orderData)
      .then(res => {
        dispatch(purchaseBurgerSuccess(res.data.name, JSON.parse(res.config.data)));
      })
      .catch(err => {
        dispatch(purchaseBurgerFail(err))
      });
  }
};
