import * as actionTypes from '../actions/actionTypes';

const initState = {
  orders: [],
  loading: false,
  error: false
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.PURCHASE_BURGER_START:
      return {
        ...state,
        loading: true
      };
    case actionTypes.PURCHASE_BURGER_SUCCESS:
      const order = {
        id: action.orderId,
        ...action.orderData
      };
      return {
        ...state,
        loading: false,
        orders: state.orders.concat(order)
      };
    case actionTypes.PURCHASE_BURGER_FAIL:
      return {
        ...state,
        error: true
      };
    default:
      return state;
  }
};

export default reducer;
